# DeepLearningChemistry-PCIC-UNAM-Invierno2018

Este proyecto es �til para predicci�n, graficci�n e interpretabilidad de materiales utilizando Deep Learning. Este es un repositorio de c�digo pr�cticamente igual al de David Duvenaud, Dougal Maclaurin y Ryan P. Adams (ver c�digo y documentaci�n en https://github.com/HIPS/neural-fingerprint) 

En cuanto a la implementaci�n, principalmente se usa Python, NumPy, RDKit y Autograd. No se usa Keras, Tensorflow, Pytorch ni similares.

Una falla importante del repositorio original es dejar de explicar el procedimiento para correr ejemplos b�sicos, por ello aqu� se describe el procedmiento.

1.- Crear un ambiente para Python 2.7
conda create -n py27 python=2.7 anaconda

2.- Activarlo

  source activate myenv  

3.- Instalar RDkit y autograd

  pip install autograd  
 conda install -c conda-forge rdkit
